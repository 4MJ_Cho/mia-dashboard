import api from '../../api';

export default {
    getAllInstructors() {
        return api.get('/auth/instructors').then(res => {
            return res;
        })
    },

    getSingleInstructor(id) {
        return api.get(`/auth/instructors/${id}`).then(res => {
            return res;
        })
    },

    addInstructor(instructor){
        return api.post('/instructors',instructor).then(res => {
            return res;
        })
    },

    updateInstructor(id,update){
        return api.patch(`/instructors/${id}`,update).then(res => {
            return res;
        })
    },

    deleteInstructor(id) {
        return api.delete(`/instructors/${id}`).then(res => {
            return res;
        })
    },

    photoUpload(fd) {
        return api.post('/upload',fd,{
        headers: {
          'Content-Type': 'multipart/form-data',
          'photolocation':'instructors'
        }
      }).then(res => {
          return res;
      })
    }
}
const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 5000;
const history = require('connect-history-api-fallback');
app.use(express.static(path.join(__dirname +"/dist")));

app.use(history({
    disableDotRule: true,
    verbose: true
}));



// app.get('/',function(req,res){
//     res.render(path.join(__dirname,"/dist/index.html"));
// })

app.get(/.*/, function (req, res) {
    res.sendFile(path.join(__dirname, '/dist/index.html'))
   })



app.listen(port,()=>{
    console.log('server running on port 5000');
});
import api from '../../api';
export default {
    getCourses() {
        return api.get('/auth/courses?select=title').then(resp => {
            return resp;
        })
    },

    getCourseFaq(courseId){
        return api.get(`/auth/course/faqs/${courseId}`).then(resp => {
            return resp;
        })
    },

    updateSingleFaq(courseId,id,data) {
        return api.patch(`/course/faqs/${courseId}/${id}`,data).then(res => {
            return res;
        })
    },

    createCourseFaq(data) {
        return api.post('/course/faqs',data).then(res => {
            return res;
        })
    },

    addCourseFaq(id,data) {
        return api.post(`/course/faqs/${id}`,data).then(res => {
            return res;
        })
    },

    deleteCourseFaq(id,courseId) {
        return api.delete(`/course/faqs/${courseId}/${id}`).then(res => {
            return res;
        })
    }
}
import api from '../../api'

export default {
    getAllAdmins() {
        return api.get('/users').then(res => {
            return res;
        })
    },

    addAdmin(data) {
        return api.post('/register',data).then(res => {
            return res;
        })
    },

    deleteAdmin(id) {
        return api.delete(`/users/delete/${id}`).then(res => {
            return res;
        })
    }   
}
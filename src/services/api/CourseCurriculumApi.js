import api from '../../api';

export default {
    getCourses() {
        return api.get('/auth/courses?select=title').then(resp => {
            return resp;
        })
    },

    getCourseCurriculum(courseId){
        return api.get(`/auth/curriculums/${courseId}`).then(resp => {
            return resp;
        })
    },

    updateSingleCurriculum(courseId,id,data) {
        return api.patch(`/curriculums/${courseId}/${id}`,data).then(res => {
            return res;
        })
    },

    createCourseCurriculum(data) {
        return api.post('/curriculums',data).then(res => {
            return res;
        })
    },

    addCourseCurriculum(id,data) {
        return api.post(`/curriculums/${id}`,data).then(res => {
            return res;
        })
    },

    deleteCourseCurriculum(id,courseId) {
        return api.delete(`/curriculums/${courseId}/${id}`).then(res => {
            return res;
        })
    }
}
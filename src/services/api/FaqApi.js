import api from '../../api'

export default {
    getFaq() {
        return api.get('/auth/faqs').then(res => {
            return res;
        })
    },

    getSingleFaq(id) {
        return  api.get(`/auth/faqs/${id}`).then(res => {
            return res;
        })
    },

    addFaq(faq) {
        return api.post('/faqs',faq).then(res =>{
            return res;
        })
    },

    updateFaq(id,update) {
        return api.patch(`/faq/${id}`,update).then(res => {
            return res;
        })
    },

    deleteFaq(id) {
        return  api.delete(`/faq/${id}`).then(res => {
            return res;
        })
    } 
}
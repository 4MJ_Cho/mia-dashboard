import axios from "axios";
const baseUrl = process.env.VUE_APP_API_BASE_URL;

export default axios.create({
 baseURL: `${baseUrl}`, //`http://localhost:8070/`,
 headers: {
 "Accept": "application/json",
 "Content-Type": "application/json"
    }
})

// baseURL: `https://sevenaaa-api.herokuapp.com`,
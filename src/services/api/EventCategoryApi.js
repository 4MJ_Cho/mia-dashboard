/**
* Description of the file. Add link pointers with {@link link
    name}
    * @author Donacien/Owner name
    * @date 13/08/2020
    * Contributors : Dzekewong Karlson,
**/
import api from '../../api';

export default {
 getAllEventsCategories(){
    return api.get('/auth/events/categories')
    .then(res =>{
        return res.data.data
    })
        },
 addEventsCategory(data){
    return api.post('/events/categories',data)
    .then(res =>{
         return res.data.success
    })
},
 updateEventCategory(id,data){
   return api.patch(`/events/category/${id}`,data)
    .then(res =>{
        return res.data
    })   
},
 deleteEventsCategory(id){
  return  api.delete(`/events/category/${id}`)
    .then(res =>{
        return res.data
    })
}
}

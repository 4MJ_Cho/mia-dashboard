import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')


// Views - Components
const Admission = () => import('@/views/pages/Admission')//DONE

const Home = () => import('@/views/pages/Home')//DONE

const About = () => import('@/views/pages/About')//DONE

const Contact = () => import('@/views/pages/Contact')//DONE

const FaqPage = () => import('@/views/pages/FaqPage')//DONE

const SeoPage = () => import('@/views/pages/Seo')//DONE

const BlogPage = () => import('@/views/pages/Blog')

const EventsPage = () => import('@/views/pages/Events')

// Views - Buttons
const AllCertification = () => import('@/views/certification/AllCertification')//SET
const AddNewCertification = () => import('@/views/certification/AddNewCertification')//SET


const AllCourses= () => import('@/views/course/AllCourses')//SET
const AddNewCourse = () => import('@/views/course/AddNewCourse')//SET
const UpdateCourse = () => import('@/views/course/UpdateCourse')//SET

 const AddNewFaq = () => import('@/views/faq/AddNewFaq')//SET

 const Banner = () => import('@/views/banner/Banner')//SET
 const Info = () => import('@/views/info/Info')//SET

 const AddNewCourseFaq = () => import('@/views/course-faq/AddNewCourseFaq')//SET


const AddNewCourseCurriculum = () => import('@/views/corriculum/AddNewCourseCurriculum')//SET

const AllAdmins = () => import('@/views/admin/AllAdmins')//SET
const AddNewAdmin = () => import('@/views/admin/AddNewAdmin')//SET


const AllInstructors = () => import('@/views/instructors/AllInstructors')//SET
const AddNewInstructor = () => import('@/views/instructors/AddNewInstructor')//SET


const AllTestimonial = () => import('@/views/testimonials/AllTestimonial')//SET
const AddNewTestimonial = () => import('@/views/testimonials/AddNewTestimonial')//SET


// Views - Icons
const AllEvents = () => import('@/views/events/AllEvents')
const AddNewEvent = () => import('@/views/events/AddNewEvent')
const UpdateEvent = () => import('@/views/events/UpdateEvent')

const EVentCategory = () => import('@/views/events/EventCategory')//SET

// Views - Notifications
const AllPosts = () => import('@/views/blog/AllPosts')//SET
const AddNewPosts = () => import('@/views/blog/AddNewPosts')//SET
const PostsCategory = () => import('@/views/blog/PostCategory')//SET
const EditPost = () => import('@/views/blog/EditPost')//
// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const ResetPassword = () => import('@/views/pages/ResetPassword')
const ForgotPassword = () => import('@/views/pages/ForgotPassword')


Vue.use(Router)

export default new Router({
  mode: 'hash', 
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [
    {
      path: '/auth',
      redirect: '/auth/login',
      name: 'Login',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '/auth/500',
          name: 'Page500',
          component: Page500
        },
        {
          path: '/auth/login',
          name: 'Login',
          component: Login,
          beforeEnter(to, from, next) {
            if(store.getters.isLoggedIn){
              next({
                name: "Dashboard" 
              });
            }else {
              next()
            }
          }
        },
        {
          path: '/auth/reset_password',
          name: 'ResetPassword',
          component: ResetPassword,
          beforeEnter(to, from, next) {
            if(store.getters.isLoggedIn){
              next({
                name: "Dashboard" 
              });
            }else {
              next()
            }
          }
        },
        {
          path: '/auth/forgot_password',
          name: 'ForgotPassword',
          component: ForgotPassword,
          beforeEnter(to, from, next) {
            if(store.getters.isLoggedIn){
              next({
                name: "Dashboard" 
              });
            }else {
              next()
            }
          }
        },
      ]
    },
    {
      path: '/',
      redirect: '/pages/home',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          redirect: '/pages/home',
          component: Dashboard,
          meta: {
            requiresAuth: true
          },
        },
        {
          path: 'pages',
          // redirect: '/pages/home',
          name: 'Pages',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'admission',
              name: 'Admission',
              component: Admission,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'home',
              name: 'Home',
              component: Home,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'about',
              name: 'About Us',
              component: About,
              meta: {
                requiresAuth: true
              },
            },
            {
               
              path: 'contact',
              name: 'Contact Us',
              component: Contact,
              meta: {
                requiresAuth: true
              },
              
            },
            {
               
              path: 'faq',
              name: 'Faq',
              component: FaqPage,
              meta: {
                requiresAuth: true
              },
              
            },
            {
               
              path: 'seo',
              name: 'Seo',
              component: SeoPage,
              meta: {
                requiresAuth: true
              },
              
            },
            {
              path: 'blog',
              name: 'Blog',
              component: BlogPage,
              meta: {
                requiresAuth: true
              }
            },
            {
              path: 'events',
              name: 'Events',
              component: EventsPage,
              meta: {
                requiresAuth: true
              }
            }
          ]
        },
        {
          path: 'certification',
          redirect: '/certification/all',
          name: 'Certification',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All Certification',
              component: AllCertification,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New Certification',
              component: AddNewCertification,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'faq',
          redirect: '/faq/add-new',
          name: 'Faq',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'add-new',
              name: 'Add New',
              component: AddNewFaq,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'banner',
          redirect: '/banner/add-new',
          name: 'Faq',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'add-new',
              name: 'UPDATE BANNER',
              component: Banner,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'info',
          redirect: '/info/add-new',
          name: 'Info',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'add-new',
              name: 'UPDATE Info',
              component: Info,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'coursefaq',
          redirect: '/coursefaq/add-new',
          name: 'Certification',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'add-new',
              name: 'Add New',
              component: AddNewCourseFaq,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'events',
          redirect: '/events/all',
          name: 'CoreUI Icons',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All Events',
              component: AllEvents,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New Event',
              component: AddNewEvent,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'category',
              name: 'Add New Category',
              component: EVentCategory,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'update',
              name: 'UpdateEvent',
              component: UpdateEvent,
              meta: {
                requiresAuth: true
              },
            },
            
          ]
        },
        {
          path: 'posts',
          redirect: '/posts/all',
          name: 'Blog Post',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All Posts',
              component: AllPosts,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New Posts',
              component: AddNewPosts,
              meta: {
                requiresAuth: true
              },
            },     {
              path: 'edit/:id',
              name: 'editPost',
              props: true,
              component: EditPost,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'category',
              name: 'Posts Category',
              component: PostsCategory,
              meta: {
                requiresAuth: true
              },
            }
          ]
        },
        {
          path: 'course',
          redirect: '/course/all',
          name: 'Course',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All Courses',
              component: AllCourses,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New',
              component: AddNewCourse,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'update/:slug',
              name: 'UpdateCourse',
              component: UpdateCourse,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'curriculum',
          redirect: '/curriculum/all',
          name: 'Course Curriculum',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'add-new',
              name: 'Curriculum',
              component: AddNewCourseCurriculum,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'instructors',
          redirect: '/instructors/all',
          name: 'All Instructors',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All Instructors',
              component: AllInstructors,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New Course',
              component: AddNewInstructor,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'testimonials',
          redirect: '/testimonials/all',
          name: 'All testimonials',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All testimonials',
              component: AllTestimonial,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New Testimonial',
              component: AddNewTestimonial,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
        {
          path: 'users',
          redirect: '/users/all',
          name: 'Admin Users',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'all',
              name: 'All Users',
              component: AllAdmins,
              meta: {
                requiresAuth: true
              },
            },
            {
              path: 'add-new',
              name: 'Add New Admin',
              component: AddNewAdmin,
              meta: {
                requiresAuth: true
              },
            },
          ]
        },
      ]
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: '/404',
      name: 'Page404',
      component: Page404
    },
  ]
}


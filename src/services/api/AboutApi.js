import api from '../../api';
export default {
    getAboutPage() {
        return api.get('/auth/about').then(res => {
            return res;
        })
    },

    createAboutPage(about) {
        return api.post('/about',about,{
            headers: {
              'Content-Type': 'application/json',
            }
          }).then(res => {
              return res;
          })
    },  

    updateAboutPage(id,update) {
        return api.patch(`/about/${id}`,update).then(res => {
            return res;
        })
    },

    photoUpload(fd) {
        return api.post('/upload',fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'about'
            }
          }).then(res => {
              return res;
          })
    }
}
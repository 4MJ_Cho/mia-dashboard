import api from '../../api';
export default {
    getAdmissionPage() {
        return api.get('/auth/admission').then(res => {
            return res;
        })
    },

    createAdmissionPage(admission) {
        return api.post('/admission',admission,{
            headers: {
              'Content-Type': 'application/json',
            }
            }).then(res => {
                return res;
            })
    },

    updateAdmissionPage(id,update) {
        return api.patch(`/admission/${id}`, update, {
        headers: {
          'Content-Type':'application/json'
        }
      }).then(res => {
          return res;
      })
    },
    photoUpload(fd) {
        return api.post('/upload',fd,{
        headers: {
          'Content-Type': 'multipart/form-data',
          'photolocation':'admission'
        }
      }).then(res => {
          return res;
      })
    } 
}
module.exports = {
  publicPath: process.env.APP_BASE_PATH,
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    //Necessary to run npm link https://webpack.js.org/configuration/resolve/#resolve-symlinks
    resolve: {
       symlinks: false
    }
  },
  transpileDependencies: [
    '@coreui/utils'
  ]
}
// module.exports = {
//   devServer: {
//       watchOptions: {
//           poll: true
//       }
//   }
// }

import api from '../../api';

export default {
    getBanner() {
        return api.get('/auth/banner').then(res => {
            return res;
        })
    },

    createBanner(bannaData) {
        return  api.post('/banner',bannaData,{
            headers: {
              'Content-Type': 'application/json',
            }
          }).then(res => {
              return res;
          })
    },

    updateBanner(id,update) {
        return api.patch(`/banner/${id}`,update).then(res => {
            return res;
        })
    },

    photoUpload(fd) {

        return api.post('/upload',fd,{
        headers: {
          'Content-Type': 'multipart/form-data',
          'photolocation':'banner'
        }
      }).then(res => {
          return res;
      })
    } 
}
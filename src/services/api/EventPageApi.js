import api from '../../api';
export default {
    getEventPage() {
        return api.get('/auth/eventpage').then(res => {
            return res;
        })
    },
    createEventPage(eventpage) {
        return api.post('/eventpage', eventpage, {
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(res => {
            return res;
        })
    },

    updateEventPage(id, update) {
        return api.patch(`/eventpage/${id}`, update)
            .then(res => {
                return res;
            })
    },
    photoUpload(fd) {
        return api.post('/upload', fd, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'photolocation': 'event'
            }
        }).then(res => {
            return res;
        })
    }
}

import api from '../../api';
export default {
    getBlogPage() {
        return api.get('/auth/blogpage').then(res => {
            return res;
        })
    },
    createBlogPage(blog) {
        return api.post('/blogpage', blog, {
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(res => {
            return res;
        })
    },

    updateBlogPage(id, update) {
        return api.patch(`/blogpage/${id}`, update)
            .then(res => {
                return res;
            })
    },
    photoUpload(fd) {
        return api.post('/upload', fd, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'photolocation': 'blog'
            }
        }).then(res => {
            return res;
        })
    }
}

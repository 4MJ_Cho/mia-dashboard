import api from '../../api';
const baseUrl = process.env.VUE_APP_API_BASE_URL;

export default {
     getAllTestimonial() {
        return api.get('/auth/testimonials',{
        headers: {
            'Content-Type': 'application/json',
        }
        }).then(res => {
            return res;
        })
     },

     getSingleTestimonial(id) {
        return api.get(`/auth/testimonials/${id}`).then(res => {
            return res;
        })
     },

     addTestimonial(testimonial) {
        return api.post(`${baseUrl}/testimonials`,testimonial,{
            headers: {
              'Content-Type': 'application/json',
            }
        }).then(res => {
            return res;
        })
     },

     updateTestimonial(id,update){
        return api.patch(`/testimonials/${id}`,update).then(res => {
            return res;
        })
     },

     deleteTestimonial(id) {
        return api.delete(`/testimonials/${id}`).then(res => {
            return res;
        })
     },

     photoUpload(fd) {
        return api.post('/upload',fd,{
        headers: {
          'Content-Type': 'multipart/form-data',
          'photolocation':'testimonial'
        }
      }).then(res => {
          return res;
      })
    }
}
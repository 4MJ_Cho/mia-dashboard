import api from '../../api';
export default {
    getFaqPage() {
        return api.get('/auth/faqpage').then(res => {
            return res;
        })
    },
    createFaqPage(faqpage) {
        return api.post('/faqpage',faqpage).then(res => {
              return res;
          })
    },

    updateFaqPage(id,update){
        return api.patch(`/faqpage/${id}`, update)
        .then(res => {
            return res;
        })
    },
    photoUpload(fd) {
        return api.post('/upload',fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'faq'
            }
          }).then(res => {
              return res;
          })
    } 
}
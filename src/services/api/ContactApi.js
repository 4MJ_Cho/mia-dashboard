import api from '../../api';
export default {
    getContactPage() {
        return api.get('/auth/contact').then(res => {
            return res;
        })
    },
    createContactPage(contact) {
        return api.post('/contact',contact,{
            headers: {
              'Content-Type': 'application/json',
            }
          }).then(res => {
              return res;
          })
    },

    updateContactPage(id,update){
        return api.patch(`/contact/${id}`, update)
        .then(res => {
            return res;
        })
    },
    photoUpload(fd) {
        return api.post('/upload',fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'contact'
            }
          }).then(res => {
              return res;
          })
    } 
}
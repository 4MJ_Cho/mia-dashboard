import "vue-toast-notification/dist/theme-default.css";
import api from '../../api';

export default {
    getAllCertification() {
        return  api.get('/auth/certifications').then(res => {
            return res;
        })
    },

    getSingleCertification(id) {
        return api.get(`/auth/certification/${id}`).then(res => {
            return res;
        })
    },

    addCertification(certification) {
        return api.post('/certifications',certification,{
            headers: {
              'Content-Type': 'application/json',
            }
          }).then(res => {
              return res;
          })
    },

    updateCertification(id,update) {
        return api.patch(`/certification/${id}`,update).then(res => {
            return res;
        })
    },
    photoUpload(fd) {
        return api.post('/upload',fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'certification'
            }
          }).then(res => {
              return res;
          })
    },

    deleteCertification(id) {
        return  api.delete(`/certification/${id}`).then(res => {
            return res;
        })
    },


}
// import api from 'api';
import api from '../../api';

export default {
    getHome() {
        return api.get('/auth/home').then(res => {
            return res.data.home;
        });
    },

    insertHome(home) {
        return api.post('/',home,{
            headers: {
              'Content-Type': 'application/json',
            }
          }).then(resp => {
              return resp;
          })
    },

    updateHome(pageId,homeUpdate) {
        return api.patch(`/${pageId}`,homeUpdate).then(res=> {
            return res.data.home;
        })
    },
    fileUpload(fd) {
        return api.post('/upload',fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'home'
            }
          }).then(resp => {
              return resp;
          })
    }
}

import api from '../../api';

export default {
    getInfo() {
        return api.get('/auth/info').then(res => {
            return res;
        })
    },

    createInfo(data) {
        return api.post('/info',data).then(res => {
              return res;
          })
    },

    updateInfo(id,data) {
        return api.patch(`/info/${id}`,data).then(res => {
            return res;
        })
    }
}

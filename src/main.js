import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'

import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueToast from 'vue-toast-notification';
import VModal from 'vue-js-modal'
import VueResource from "vue-resource";

import api from './api';

// api.defaults.baseURL = 'https://sevenaaa-api.herokuapp.com'

Vue.prototype.$http = api;

const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = token;
}

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/auth/login')
  } else {
    next()
  }
})


Vue.use(VueResource);

api.interceptors.request.use(function (config) {
  // Do something before request is sent
  // console.log(config.headers)
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
})

api.interceptors.response.use(
  response => {

    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  error => {
    if (error.response.status === 401) {
      // alert('token expired')
      // console.log('Got here')
      store.dispatch("logout");
    }
  }
);


Vue.use( CKEditor );
// Vue.use( ClassicEditor );
Vue.use(Vuetify);
Vue.use(VueToast);
Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)
Vue.use(VModal)
new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  }
})

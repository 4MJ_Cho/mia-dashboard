// import axios from '../../axios'
import axios from 'axios'
const baseUrl = process.env.VUE_APP_API_BASE_URL;

export default {
    getSeo() {
        return axios.get(`${baseUrl}/auth/metainfo`).then(res => {
            return res.data;
        })
    },

    addSeo(seo) {
        return axios.post(`${baseUrl}/metainfo`,seo).then(res =>{
            return res;
        })
    },

    updateSeo(id,update) {
        return axios.patch(`${baseUrl}/metainfo/${id}`,update).then(res => {
            return res;
        })
    },

    photoUpload(fd) {
        return axios.post(`${baseUrl}/upload`,fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'meta'
            }
          }).then(res => {
              return res;
          })
    } 

}
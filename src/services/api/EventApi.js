/**
* Description of the file. Add link pointers with {@link link
    name}
    * @author Dzekewong Karlson/Owner name
    * @date 13/08/2020
    * Contributors : contributor name,
**/
import api from '../../api';
const baseUrl = process.env.VUE_APP_API_BASE_URL;

export default{
getAllEvents () {
    return api.get('/auth/events')
    .then(res =>{
        return res.data
    })
},
getEvents (Id){
    return api.get(`/auth/events/${Id}`)
    .then(res =>{
        return res.data
    })
},
 fileUpload (fd) { //https://sevenaaa-api.herokuapp.com
    return api.post(`${baseUrl}/upload`, fd,{
        headers: {
                'Content-Type': 'multipart/form-data',
                'photolocation':'event'
        }
        }).then(res => {
            return res.data.imgPath;
        })
    },
 addEvents (data){
    return api.post('/events',data)
            .then(res =>{
                return res.data
            })
},
updateEvents (id,data){
        return api.patch(`/event/${id}`,data)
            .then(res =>{
                return res.data
        })    
},
deleteEvent (Id){
    return api.delete(`/event/${Id}`)
    .then(res =>{
        return res.data
    })
}
}

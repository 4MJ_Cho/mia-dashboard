export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'Dashboard',
        to: '/dashboard',
        icon: 'cil-speedometer',
      },
      {
        _name: 'CSidebarNavTitle',
        _children: ['Model Initiative of Africa']
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Pages',
        route: '/pages',
        icon: 'cil-puzzle',
        items: [
          {
            name: 'Home',
            to: '/pages/home'
          },
          {
            name: 'Admission',
            to: '/pages/admission'
          },
          {
            name: 'About Us',
            to: '/pages/about'
          }, 
          {
            name: 'Contact Page',
            to: '/pages/contact',
          },
          {
            name: 'Faq Page',
            to: '/pages/faq',
          },
          {
            name: 'Global SEO',
            to: '/pages/seo',
          },
          {
            name: 'Blog',
            to: '/pages/blog'
          },
          {
            name: 'Events',
            to: '/pages/events'
          }
         
        ]
      },//END
      {
        _name: 'CSidebarNavDropdown',
        name: 'Certification',
        route: '/certification',
        icon: 'cil-cursor',
        items: [
          {
            name: 'All Certification',
            to: '/certification/all'
          },
          {
            name: 'Add New',
            to: '/certification/add-new',
          }
        ]
      },
        {
        _name: 'CSidebarNavDropdown',
        name: 'Faq',
        route: '/faq',
        icon: 'cil-cursor',
        items: [
          {
            name: 'Add New',
            to: '/faq/add-new',
          }
        ]
      },
        {
        _name: 'CSidebarNavDropdown',
        name: 'Course Faq',
        route: '/coursefaq',
        icon: 'cil-cursor',
        items: [
          {
            name: 'Add New',
            to: '/coursefaq/add-new',
          }
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Banner',
        route: '/banner',
        icon: 'cil-cursor',
        items: [
          {
            name: 'Add New',
            to: '/banner/add-new',
          }
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Top Info',
        route: '/info',
        icon: 'cil-cursor',
        items: [
          {
            name: 'Add New',
            to: '/info/add-new',
          }
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Events',
        route: '/events',
        icon: 'cil-star',
        items: [
          {
            name: 'All Events',
            to: '/events/all',
          },
          {
            name: 'Add New',
            to: '/events/add-new',
            // badge: {
            //   color: 'primary',
            //   text: 'NEW'
            // }
          },
          {
            name: 'Event Category',
            to: '/events/category',
            // badge: {
            //   color: 'primary',
            //   text: 'NEW'
            // }
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Post',
        route: '/posts',
        icon: 'cil-bell',
        items: [
          {
            name: 'All Posts',
            to: '/posts/all'
          },
          {
            name: 'Add New',
            to: '/posts/add-new',
            badge: {
              color: 'primary',
              text: 'NEW',
              shape: 'pill'
            }
          },
          {
            name: 'Post Category',
            to: '/posts/category'
          }
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Course',
        route: '/course',
        icon: 'cil-star',
        items: [
          {
            name: 'All Courses',
            to: '/course/all',
          },
          {
            name: 'Add New',
            to: '/course/add-new',
          },
          {
            name: 'Update',
            to: '/course/update',
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Course Corriculum',
        to: '/curriculum',
        icon: 'cil-calculator',
        items: [
          {
            name: 'Curriculum',
            to: '/curriculum/add-new',
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Course Instructors',
        to: '/instructors',
        icon: 'cil-user',
        items: [
          {
            name: 'All',
            to: '/instructors/all'
          },
          {
            name: 'Add New',
            to: '/instructors/add-new',
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Testimonials',
        to: '/testimonials',
        icon: 'cil-user',
        items: [
          {
            name: 'All',
            to: '/testimonials/all'
          },
          {
            name: 'Add New',
            to: '/testimonials/add-new',
          },
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Admin Users',
        to: '/users',
        icon: 'cil-user',
        items: [
          {
            name: 'All',
            to: '/users/all'
          },
          {
            name: 'Add New',
            to: '/users/add-new',
          },
        ]
      },
    ]
  }
]


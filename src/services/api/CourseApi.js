import api from '../../api';

export default {
    getCoursesTitle() {
        return api.get('/auth/courses?select=title,slug,author,created_At,updated_At').then(resp => {
            return resp.data;
        })
    },
    getCourseBySlug(slug) {
        return api.get(`/auth/course/${slug}`).then(res => {
            return res.data;
        })
    },
    insertCourse(course) {
        return api.post('/course',course).then(resp => {
            return resp;
        })
    },

    updateCourse(id,courseUpdate) {
        return api.patch(`/course/${id}`,courseUpdate).then(res =>{
            return res;
        })
    },
    deleteCourse(id) {
        return api.delete(`/course/${id}`).then(res => {
            console.log(res)
        })
    },
    fileUpload(fd) {
    return api.post('/upload', fd,{
        headers: {
                'Content-Type': 'multipart/form-data',
                'photolocation':'clogos'
        }
        }).then(res => {
            return res.data.imgPath;
        })
    },
    mainUpload(fd) {
        return api.post('/upload', fd,{
            headers: {
              'Content-Type': 'multipart/form-data',
              'photolocation':'cmain'
          }
        }).then(res => {
            return res.data.imgPath;
        })
    }
}
